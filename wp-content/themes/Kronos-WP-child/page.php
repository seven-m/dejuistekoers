<?php get_header(); ?>
<!-- <div class="ow-overlay"></div> -->
<div id="go-up-button"></div>
<div class="center-relative content-960">  
    <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
            <?php
            $image_thumb = get_post_meta($post->ID, "single_image_thumb", true);
            $vimeo_thumb = get_post_meta($post->ID, "single_vimeo_thumb", true);
            $html_thumb = get_post_meta($post->ID, "single_html_thumb", true);
            $title = get_post_meta($post->ID, "single_custom_title", true) ? get_post_meta($post->ID, "single_custom_title", true) : get_the_title() ;
            $subtitle = get_post_meta($post->ID, "single_subtitle", true);
            $show_page_title = get_post_meta($post->ID, "single_show_page_title", 'yes');
            $has_thumb = '0';

            if ($image_thumb != '') {
                $single_thumb = '<img class="top-round" src="' . $image_thumb . '" alt="" />';
                $has_thumb = '1';
            }

             if ($vimeo_thumb != '') {
				$is_vimeo = strstr($vimeo_thumb, 'vimeo.com/');
				if($is_vimeo == false){							
				$get_vimeo_thumb = explode('youtube.com/watch?v=', $vimeo_thumb);
				$single_thumb = '<div class="youtube">             
							<input type="hidden" class="youtube_source" value="'.$get_vimeo_thumb[1].'"/> 											
                                </div>';														
				}else{					
				$get_vimeo_thumb = explode('vimeo.com/', $vimeo_thumb);				
				$single_thumb = '<div class="vimeo">
                        	<input type="hidden" class="vimeo_source" value="'.$get_vimeo_thumb[1].'"/>       
                                </div>';
				}
                $has_thumb = '1';
				}            

            if ($html_thumb != '') {
                $single_thumb = str_replace("&", "&amp;", $html_thumb);
                $has_thumb = '1';
            }

            if ($has_thumb == '0') {
                $single_thumb = '';
            }
            ?>
            <article id="single-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="single-top-thumb"><?php echo do_shortcode($single_thumb); ?></div>
                <div class="article-content">
                    <!--
                    <a class="popup-close">
                        <div class="image-holder">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/close_icon.png" alt="" class="absolute" />
                        </div>
                    </a>
                    -->
                    <?php if ($show_page_title != 'no') : ?>
                        <header class="page-title entry-header">
                            <div class="page-title"><?php echo $subtitle; ?></div>
                            <h1 class="title-description entry-title"><?php echo $title; ?></h1>
                        </header><!-- End of entry-header -->
                    <?php endif; ?>
                    <div class="entry-content">
                        <?php
                            the_content();
                            edit_post_link( __( 'Edit', 'kronos-wp' ), '<span class="edit-link">', '</span>' );
                        ?>
                    </div><!-- .entry-content -->
                    <div class="right tags"><?php the_tags(); ?></div>
                    <div class="clear"></div>
                    <?php wp_link_pages('before=<div id="page-links">&after=</div>'); ?>						
                </div>
                <div class="clear"></div>
            </article><!-- end article -->
        <?php endwhile; ?>			
    <?php endif; ?>
</div>
<?php get_footer(); ?>