<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Kronos-WP-child
 */

 //Get custom field values
    $show_page_title = get_post_meta($post->ID, "page_title", true);
    $page_title = get_post_meta($post->ID, "page_custom_page_title", true) ? get_post_meta($post->ID, "page_custom_page_title", true) : get_the_title($post);
    $page_title_description = get_post_meta($post->ID, "page_title_description", true);
    $page_title_color = get_post_meta($post->ID, "page_title_color", true);

	$slug = $post->ID;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php if ($show_page_title != 'no') : ?>
        <header class="page-title entry-header">
            <h2 class="page-title entry-title"><?php echo $page_title; ?></h2>
            <div class="title-description"><?php echo $page_title_description; ?></div>
        </header><!-- End of entry-header -->
    <?php endif; ?>
    <div class="entry-content">
        <?php
            the_content();
            edit_post_link( __( 'Edit', 'kronos-wp' ), '<span class="edit-link">', '</span>' );
        ?>
    </div><!-- .entry-content -->
</article><!-- #post-## -->