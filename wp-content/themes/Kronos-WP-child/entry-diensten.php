<?php
/**
 * The template used for displaying diensten item
 *
 * @package WordPress
 * @subpackage Kronos-WP-child
 */
global $post;

if (has_post_thumbnail($post->ID)) {
    $thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
} else {
    $thumb_url = get_template_directory_uri().'/images/no_photo.jpg';
}

if (has_post_thumbnail($post->ID)) {
    $portfolio_post_thumb = get_the_post_thumbnail();
} else {
    $portfolio_post_thumb = '<img src = "' . get_template_directory_uri() . '/images/no_photo.jpg" alt = "" />';
}
$contents = get_extended(get_the_content());
$intro_text = get_post_meta($post->ID, "page_excerpt", true) ? get_post_meta($post->ID, "page_excerpt", true) : substr(strip_tags($contents['main']),0,50).'...';
?>
<li class="diensten-item">
    <article>
        <div class="photo-holder">
            <a href="<?php echo get_permalink($post->ID); ?>">
                <?php echo $portfolio_post_thumb; ?>    
            </a>
        </div>
        <header>
            <h2 class="diensten-item-title">
                <a href="<?php echo get_permalink($post->ID); ?>">
                    <?php echo get_the_title($post->ID); ?>
                </a>
            </h2>
        </header>
        <section>
            <div class="entry-content">
                <?php echo $intro_text; ?>
            </div>
            <div class="entry-button">
                <style>
                    a.diensten-button.id-<?php echo $post->ID?>::after {background-image: url(<?php echo $thumb_url[0]; ?>);}
                </style>
                <a class="button diensten-button id-<?php echo $post->ID?>" href="<?php echo get_permalink($post->ID); ?>" title="<?php echo get_the_title($post->ID); ?>">
                    <?php echo __('Klik hier', 'Kronos-wp-child'); ?>
                </a>
            </div>
        </section>
        <div class="clear"></div>
    </article>
</li>