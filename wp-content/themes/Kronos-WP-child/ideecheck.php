<?php 
/*
* Template Name: Ideecheck
*/
function kwp_enqueue_ideecheck_scripts() {
    wp_enqueue_script( 'ideecheck_script', get_stylesheet_directory_uri() . '/js/ideecheck.js', array('jquery'),'1.0');
}
add_action( 'wp_enqueue_scripts', 'kwp_enqueue_ideecheck_scripts' );
function kwp_enqueue_ideecheck_styles() {
    wp_enqueue_style( 'ideecheck_styles', get_stylesheet_directory_uri() . '/ideecheck.css');
}
add_action( 'wp_head', 'kwp_enqueue_ideecheck_styles' );
get_header(); 
?>
<!-- <div class="ow-overlay"></div> -->
<div id="go-up-button"></div>
<div id="ideecheck-overlay"></div>
<div id="ideecheck-contact-form">
    <div class="inner">
        <h1 class="title">Doe de gratis ideecheck</h1>
        <div class="description">Laat hier je naam en e-mailadres achter om de gratis check te doen.</div>
        <div>
            <?php echo do_shortcode("[contact-form submit_button_text='Verstuur'][contact-field label='Naam' type='name' default='naam' required='1'/][contact-field label='E-mailadres' type='email' default='e-mailadres' required='1'/][/contact-form]"); 
            ?>
        </div>
    </div>
</div>
<div class="center-relative content-960">  
    <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
            <?php
            $title = get_post_meta($post->ID, "page_custom_title", true) ? get_post_meta($post->ID, "single_custom_title", true) : get_the_title() ;
            $subtitle = get_post_meta($post->ID, "page_title_description", true);
            $background_color = get_post_meta($post->ID, "page_background_color", '#ffffff');
            $show_page_title = get_post_meta($post->ID, "page_show_page_title", 'yes');
            ?>
            <style>
                body {background: <?php echo $background_color; ?> !important; }
            </style>
            <article id="single-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="article-content">
                    <?php if ($show_page_title != 'no') : ?>
                        <header class="page-title entry-header">
                            <div class="page-title"><?php echo $subtitle; ?></div>
                            <h1 class="title-description entry-title"><?php echo $title; ?></h1>
                        </header><!-- End of entry-header -->
                    <?php endif; ?>
                    <div class="entry-content">
                        <?php
                            the_content();
                            edit_post_link( __( 'Edit', 'kronos-wp' ), '<span class="edit-link">', '</span>' );
                        ?>
                    </div><!-- .entry-content -->
                    <div class="right tags"><?php the_tags(); ?></div>
                    <div class="clear"></div>
                    <?php wp_link_pages('before=<div id="page-links">&after=</div>'); ?>						
                </div>
                <div class="clear"></div>
            </article><!-- end article -->
        <?php endwhile; ?>			
    <?php endif; ?>
</div>
<?php get_footer(); ?>