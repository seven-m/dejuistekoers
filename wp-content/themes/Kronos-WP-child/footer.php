<!-- Footer -->
<footer class="clear center-text"> 
    <div class="center-relative content-960">
        <!--
        <?php if (get_option('dry_footer_logo')): ?>
            <div class="footer-logo"><img src="<?php echo get_option('dry_footer_logo'); ?>" alt="" /></div>
        <?php endif; ?>
        -->
        <?php dynamic_sidebar('footer'); ?>
        <div class="site-info">
            <?php echo get_theme_mod('copyright_text', '&copy; DryThemes - Built with love and passion'); ?>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>