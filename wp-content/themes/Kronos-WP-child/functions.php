<?php
/**
 * @package Kronos-WP Child
 *
*/
function kronos_wp_child_enqueue_main_styles() {
    //wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
    wp_enqueue_style( 'child_style',
        get_stylesheet_directory_uri() . '/style.css',
        array('clear_style','common_style','colors_style','default_style','columns_style','typography_style','responsive_style','main_theme_style')
    );
}
add_action( 'wp_enqueue_scripts', 'kronos_wp_child_enqueue_main_styles' );

// add editor style
function kronos_wp_child_theme_add_editor_styles() {
    add_editor_style( 'style.css' );
}
add_action( 'admin_init', 'kronos_wp_child_theme_add_editor_styles' );

// register footers
add_action( 'widgets_init', 'kronos_wp_child_slug_widgets_init' );
function kronos_wp_child_slug_widgets_init() {
    register_sidebar( array(
            'name' => __( 'Footer', 'kronots-wp-child' ),
            'id' => 'footer',
            'description' => __( 'Widgets for the footer area.', 'kronos-wp-child' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        ) 
    );
}

add_action( 'after_setup_theme', 'kronos_wp_child_register_sticky_menu' );
function kronos_wp_child_register_sticky_menu() {
  register_nav_menu( 'sticky', __( 'Sticky Menu', 'theme-slug' ) );
}

function kronos_wp_child_customize_register( $wp_customize )
{
   $wp_customize->add_setting('blogdescriptionlong', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type'           => 'option'
    ));
 
    $wp_customize->add_control('blogdescriptionlong', array(
        'label'      => __('Blog description long', 'kronos_wp'),
        'section'    => 'title_tagline',
        'settings'   => 'blogdescriptionlong',
    ));
    
}
add_action( 'customize_register', 'kronos_wp_child_customize_register' );

// remove styles and scripts
add_action('wp_head', 'kwp_remove_scripts_styles', 100);
function kwp_remove_scripts_styles() {
    wp_dequeue_script( 'smallMenu' );
    wp_dequeue_style( 'google_fonts_style' );
}


function kronos_wp_child_enqueue_styles() {
    wp_register_style('google_fonts_style_ubuntu', 'http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic');
    wp_enqueue_style('google_fonts_style_ubuntu');
    wp_register_style('google_fonts_style_lekton', 'http://fonts.googleapis.com/css?family=Lekton:400,700,400italic');
    wp_enqueue_style('google_fonts_style_lekton');
}
add_action('wp_head', 'kronos_wp_child_enqueue_styles');

/*
* ADD JAVASCRIPT 
*/ 
function kronos_wp_child_add_js() {
    $src = get_stylesheet_directory_uri().'/js/global.js';
    wp_enqueue_script( 'kwp-child-global-js', $src, array('jquery'), '1.0' );
}
add_action( 'wp_enqueue_scripts', 'kronos_wp_child_add_js');

// Add some page preference options
add_action('add_meta_boxes', 'kronos_wp_child_add_items_to_page_meta_box');
add_action('save_post', 'kronos_wp_child_add_items_to_page_meta_box');
function kronos_wp_child_add_items_to_page_meta_box() {
    global $custom_meta_fields;
    $prefix = 'page_';
    $fields = array(
        array(
            'label' => 'Don\'t show on the home page?',
            'desc' => 'If selected, this page will be excluded from the home page.',
            'id' => $prefix . 'exclude_from_homepage',
            'type' => 'checkbox'
        ),
        array(
            'label' => 'Text color style',
            'desc' => 'If the page background is dark, you want to select light text and vice versa.',
            'id' => $prefix . 'text_style',
            'type' => 'select',
            'options' => array(
                'one' => array(
                    'label' => 'Dark',
                    'value' => 'dark'
                ),
                'two' => array(
                    'label' => 'Light',
                    'value' => 'light'
                )
            )
        ),
        array(
            'label' => 'Custom page title',
            'desc' => 'Your custom page title.',
            'id' => $prefix . 'custom_page_title',
            'type' => 'text'
        )
    );
    $fields = array_reverse($fields);
    foreach($fields as $field) {
        array_unshift($custom_meta_fields, $field);
    }
}
// Add some page preference options
add_action('add_meta_boxes', 'kronos_wp_child_add_items_to_single_meta_box');
add_action('save_post', 'kronos_wp_child_add_items_to_single_meta_box');
function kronos_wp_child_add_items_to_single_meta_box() {
    global $single_custom_meta_fields;
    $prefix = 'single_';
    $fields = array( 
        array(
            'label' => 'Custom title',
            'desc' => 'Your custom page title.',
            'id' => $prefix . 'custom_title',
            'type' => 'text'
        ),
        array(
            'label' => 'Subtitle',
            'desc' => 'Your custom page subtitle.',
            'id' => $prefix . 'subtitle',
            'type' => 'text'
        ),
        array(
            'label' => 'Excerpt',
            'desc' => 'Piece of (intro)text to display on the homepage.',
            'id' => 'page_excerpt',
            'type' => 'textarea'
        ),
        array(
            'label' => 'Show page title',
            'desc' => 'Show page title or not?',
            'id' => $prefix.'show_page_title',
            'type' => 'select',
            'options' => array(
                    'one' => array(
                        'label' => 'YES',
                        'value' => 'yes'
                    ),
                    'two' => array(
                        'label' => 'NO',
                        'value' => 'no'
                    )
                )
        )
    );
    $fields = array_reverse($fields);
    foreach($fields as $field) {
        array_unshift($single_custom_meta_fields, $field);
    }
}

function kwp_shortcode_quote($atts, $content = null) {
    extract(shortcode_atts(array(
                "class" => ''
                    ), $atts));
    $return = '<span class="quote ' . $class . '">' . do_shortcode($content) . '</span>';
    return $return;
}
add_shortcode("quote", "kwp_shortcode_quote");

function custom_diensten_shortcode($atts, $content = null) {
    get_template_part('content', 'diensten');    
}
function kwp_child_theme_setup() {
    remove_shortcode("portfolio");
    add_shortcode("diensten", "custom_diensten_shortcode");
    add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'kwp_child_theme_setup' );

/* Custom styles in editor */
// Callback function to insert 'styleselect' into the $buttons array
function kronos_wp_child_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
add_filter('mce_buttons_2', 'kronos_wp_child_mce_buttons_2');
// Callback function to filter the MCE settings
function kronos_wp_child_mce_before_init_insert_formats( $init_array ) {  
	// Define the style_formats array
	$style_formats = array(  
		// Each array child is a format with it's own settings
		array(  
			'title' => 'emphasized',  
			'inline' => 'span',  
			'classes' => 'emphasized',
			'wrapper' => false,
			
		),
		array(  
			'title' => 'quote',  
			'inline' => 'span',  
			'classes' => 'quote',
			'wrapper' => true,
		),
		array(  
			'title' => 'quote - full width',  
			'inline' => 'span',  
			'classes' => 'quote full_width',
			'wrapper' => true,
		)
	);  
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array;  
  
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'kronos_wp_child_mce_before_init_insert_formats' ); 


// Class to handle homepage sections
class KwpHomePages {

    public static $pages = array();
    
    private static $loop = 0; 
    
    function __construct() {
        if(!self::$loop) {
            // load loop
            self::$loop = new WP_Query(array(
                'post_type'=>'page', 
                'posts_per_page'=>'-1', 
                'orderby'=>'menu_order', 
                'order'=>'ASC', 
                'meta_query'=> array(
                        array(
                            'key'=>'page_exclude_from_homepage', 
                            'compare'=>'NOT EXISTS'
                        )
                )
            ));
            // register pages
            global $post;
            if (self::$loop->have_posts()) { 
                while (self::$loop->have_posts()) { 
                    self::$loop->the_post(); 
                    $url = get_permalink($post->ID);
                    if (!in_array($url, self::$pages)) self::$pages[] = $url;
                }
            }
        }
    }
    public function getLoop() {
        return self::$loop;
    }
    public function getPages() {
        return self::$pages;
    }
    public function isOnHomePage($url) {
        return in_array($url, self::$pages);
    } 
}
new KwpHomePages();

// Modify Menu items URLs for homepage
add_filter( 'wp_nav_menu_objects', 'kronos_wp_child_mod_menu_links' );
function kronos_wp_child_mod_menu_links( $items ) {
    //if (!is_home()) return $items;
    $kwp_pages = new KwpHomePages();
    foreach ( $items as $item ) {
        $post = get_post(($item->object_id));
        if ($item->type != 'custom' && !get_post_meta($post->ID, "page_exclude_from_homepage", true) && $kwp_pages->isOnHomePage($item->url)) {
            $item->url = get_bloginfo('url') . '/#' . $post->post_name;
        }
    }
    return $items; 
}

/* REGISTER CUSTOM POST TYPE */
add_action('init', 'kwp_create_diensten');
function kwp_create_diensten() {
    $diensten_args = array(
        'label' => 'Diensten',
        'singular_label' => 'Dienst',
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => true,
        'supports' => array('title', 'editor', 'comments', 'custom-fields', 'thumbnail'),
        'taxonomies' => array('category', 'post_tag')
    );
    register_post_type('diensten', $diensten_args);
}
// <editor-fold defaultstate="collapsed" desc="Add support to theme for Portfolio thumbnails">
add_theme_support('post-thumbnails', array('diensten'));
// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Portfolio filter">
add_action('init', 'kwp_create_diensten_taxonomies');
function kwp_create_diensten_taxonomies() {
    $labels = array(
        'name' => 'Filter',
        'singular_name' => 'Filter',
        'search_items' => 'Search Filter',
        'all_items' => 'All Filter',
        'parent_item' => 'Parent Filter',
        'parent_item_colon' => 'Parent Filter:',
        'edit_item' => 'Edit Filter',
        'update_item' => 'Update Filter',
        'add_new_item' => 'Add New Filter',
        'new_item_name' => 'New Genre Filter',
        'menu_name' => 'Filter',
    );
    register_taxonomy('diensten_filter', array('diensten'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'diensten-filter'),
    ));
}
// add meta box to diensten post type
function kwp_add_single_custom_meta_box() {
    add_meta_box(
            'single_custom_meta_box', // $id  
            'Diensten Preferences', // $title   
            'show_single_custom_meta_box', // $callback  
            'diensten', // $page  
            'normal', // $context  
            'high'); // $priority  
}
add_action('add_meta_boxes', 'kwp_add_single_custom_meta_box');

// Convert links in the content into relevant homepage section urls
function kwp_convert_links($content) {
    $homepage = new KwpHomePages();
    $urls = $homepage->getPages();
    $replacements = array();
    $patterns = array();
    $prefix = 'href="';
    foreach ($urls as $key=>$u) {
        $patterns[] = $prefix.$u;
        $post = get_post(url_to_postid($u));
        $replacements[] = $prefix.get_bloginfo('url') . '/#' . $post->post_name;
        //$patterns[] = '/('.preg_quote($u).')[^(#|\?|\/)]/gm';
    }
    //$c = @preg_replace($patterns, $replacements, $content);
    //return $c ? $c : $content;
    return str_replace($patterns, $replacements, $content);
}
add_filter( 'the_content', 'kwp_convert_links' );

/* OVERRIDE CONTACT FORM SUCCESS MESSAGE */
function kwp_change_contactform_success_message( $msg ) {
    return 
        '<h3>' . 'Dank voor je vraag of opmerking.<br />' .  
            'Binnen 24 uur reageer ik op je bericht. <br />'.
            'Met vriendelijke groeten, <br />'.
            'Michael Breedt.' .
        '</h3>';
}
add_filter( 'grunion_contact_form_success_message', 'kwp_change_contactform_success_message' );