<?php
/**
 * The template used for displaying diensten item
 *
 * @package WordPress
 * @subpackage Kronos-WP-child
 */
$count = 0;
$args = array('post_type' => 'diensten', 'posts_per_page' => 4, 'order_by'=>'date', 'order'=>'ASC');
$loop = new WP_Query($args);
?>
<?php if ($loop->have_posts()) : ?>
    <div class="diensten_container">
        <ul id="work-items">
            <?php 
            while ($loop->have_posts()) : $loop->the_post();
                get_template_part('entry', 'diensten');
            endwhile;
            ?>
        </ul>
    </div>
<?php endif; ?>
    