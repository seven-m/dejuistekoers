/*!
 * Script for initializing globally-used functions and libs.
 *
 * @since 1.0.0
 */
/* global jQuery, ttfmakeFitVids */
(function($) {
    
    // Remove click event from menu parent items
    $(document).ready(function () {
        $('li.menu-item-has-children > a')
            .off('click')
            .on('click', function(e) {e.preventDefault()})
    });
    
    // Sticky menu responsive
    $(document).ready(function () {
        if ($('#menu-sticky').is(':hidden')) {
            $('.menu-sticky-menu-container').click(function(){
                $(this).toggleClass('opened');
            })
            $('#menu-sticky li a').click(function(e){
                $('.menu-sticky-menu-container').removeClass('opened');
            })
        }
    });
    
    // testimonial slider
    $(document).ready(function () {
        jQuery('#pl-1706')
            .after('<div class="testimonials_pagination"><a href="#" class="prev">prev</a><a href="#" class="next">next</a></div>')
            .carouFredSel({	
                responsive: true,
                width: 'variable',              
                auto: {
                    play: false,
                    pauseOnHover: true
                },
                prev: {button: '.testimonials_pagination .prev', key: 'left'},
                next: {button: '.testimonials_pagination .next', key: 'right'},
                scroll: {
                    fx: 'crossfade',
                    duration: 1000
                },
                swipe: {
                    onMouse: true,
                    onTouch: true
                }, 
                items: {
                    height: 'variable'			
                }
            });  
    });
    
    // back to top button
    $(document).ready(function () {
        /* scroll to top */
        $('div#go-up-button')
            .click(function(){
                $('body, html').animate({scrollTop:0}, 'slow');
            });    
        
        /* autoscroll go-to-top-button */
        var $arrow = $('div#go-up-button'),
            offset = parseInt($arrow.css('bottom'), 10),
            moveArrow = function() {
                var scrollTop = $(window).scrollTop();
                $arrow
                    .stop()
                    .animate({"bottom": (scrollTop * -1) + offset + "px"}, "slow");
            }
        moveArrow();
        $(window).scroll(function(){
            moveArrow();
        });
    });
    
    // Sticky menu
    $(document).ready(function () {
        var offset = 50, 
            addSticky = function() {
                if ($(window).scrollTop() > offset) {
                    $('body').addClass('sticky-mode');
                } else {
                    $('body').removeClass('sticky-mode');
                }
            }
        addSticky();
        $(window).scroll(function() {
            clearTimeout($.data(this, 'scrollTimer'));
            $.data(this, 'scrollTimer', setTimeout(function() {
                addSticky();
            }, 250));
        });
    });
    
    // Anchor links scroll
    $(document).ready(function () {
        $('a').click(function(e){
            var hash = $(this).attr('href').indexOf('#') < 0 ? 0 : $(this).attr('href').replace(/^.*?#/,'');
            if (hash) {
                $('html, body').animate({scrollTop: $('#'+hash).offset().top}, 'slow');
            }
        });
    });
    
    // Lightbox fix to image links
    $(document).ready(function () {
        $('a').click(function(e){
            var $a = $(this);
            if (!/(\.jpg|\.JPG|\.jpeg|\.JPEG|\.png|\.PNG)/.test($a.attr('href'))) return;
            e.preventDefault();
            $.swipebox( [
                { href: $a.attr('href'), title:'De Juiste Koers' }
            ] );
        });
    });
    
})(jQuery);