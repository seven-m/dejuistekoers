/*!
 * 
 *
 * @since 1.0.0
 */
/* global jQuery */
(function($) {
    
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
        }
        return "";
    }
    // Contact form
    $(document).ready(function () {
        if (!getCookie('dejuistekoers_test')) $('#ideecheck-contact-form, #ideecheck-overlay').show()
        else $('#ideecheck-contact-form, #ideecheck-overlay').hide();
        $('#ideecheck-contact-form form').submit(function(e){
            e.preventDefault();
            $('#ideecheck-contact-form, #ideecheck-overlay').fadeOut(500);
            setCookie('dejuistekoers_test','1');
        });
    });
    
    // Questions navi
    $(document).ready(function () {
        $questions = $('.quiz_section').appendTo('#quizForm');
        var counter = 1;
        $('.qmn_page_counter_message').text(counter + '/' + $questions.length);
        $('.qmn_mc_answer_wrap label').click(function() {
            var $next = $(this).parentsUntil('.quiz_section').parent().next();
            if (!$next.is(':visible')) {
                $next.fadeIn();
                counter++;
                $('.qmn_page_counter_message').text(counter + '/' + $questions.length); 
            }
        })
    });
    
    // Ondernemerstest result chart
    $(document).ready(function () {
        var ctx = document.getElementById("chart-area").getContext("2d"),
            data = [], 
            segment_i = 0,
            chart;
        $('#chart-holder').slideUp(0);
        // add chart data
        $.each(qmn_question_list, function(i,v) {
            var hints = v.hints.split(',');
                segment = {
                    i: segment_i,
                    value: 5,
                    label: hints[0] ? hints[0] : '',
                    color: hints[1] ? hints[1] : '#555555',
                    highlight: "#000000"
                };
            data.push(segment);
            $.each(v.answers, function(answer_i, answer_v) {
                var $el = $('#question'+v.question_id + '_' + (answer_i + 1));
                $el.data('chart', segment);
                $el.change(function(){
                    if (data.length) data[$(this).data('chart').i].value = v.answers[answer_i][1];
                });
            })
            segment_i++;
        });
        $('#quizForm').submit(function(e){
            e.preventDefault();
            $('#chart-holder').slideDown();
            chart = new Chart(ctx).PolarArea(data, {
                    animateRotate : false,
                    animateScale : true,
                    responsive:true
                });
        });
    });
})(jQuery);