/*!
 * 
 *
 * @since 1.0.0
 */
/* global jQuery */
(function($) {
    
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
        }
        return "";
    }
    // Contact form
    $(document).ready(function () {
        if (!getCookie('dejuistekoers_test')) $('#ideecheck-contact-form, #ideecheck-overlay').show()
        else $('#ideecheck-contact-form, #ideecheck-overlay').hide();
        $('#ideecheck-contact-form form').submit(function(e){
            e.preventDefault();
            $('#ideecheck-contact-form, #ideecheck-overlay').fadeOut(500);
            setCookie('dejuistekoers_test','1');
        });
    });
    
    // Questions navi
    $(document).ready(function () {
        $questions = $('.quiz_section').appendTo('#quizForm');
        var counter = 1;
        $('.qmn_page_counter_message').text(counter + '/' + $questions.length);
        $('.qmn_mc_answer_wrap label').click(function() {
            var $next = $(this).parentsUntil('.quiz_section').parent().next();
            if (!$next.is(':visible')) {
                $next.fadeIn();
                counter++;
                $('.qmn_page_counter_message').text(counter + '/' + $questions.length); 
            }
        })
    });
    
})(jQuery);