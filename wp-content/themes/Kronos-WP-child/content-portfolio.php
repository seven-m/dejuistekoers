<?php
/**
 * The template used for displaying portfolio item
 *
 * @package WordPress
 * @subpackage Kronos-WP-child
 */
$count = 0;
$args = array('post_type' => 'portfolio', 'posts_per_page' => -1);
$loop = new WP_Query($args);
?>
<?php if ($loop->have_posts()) : ?>
    <div class="portfolio_container">
        <ul id="work-items">
            <?php 
            while ($loop->have_posts()) : $loop->the_post();
                get_template_part('entry', 'portfolio');
            endwhile;
            ?>
        </ul>
    </div>
<?php endif; ?>
    