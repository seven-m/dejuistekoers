<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>        
        <meta charset="<?php bloginfo('charset'); ?>" />        
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title><?php bloginfo('name') . wp_title(); ?> | <?php bloginfo('description'); ?></title>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />  		
		<?php if (get_option('dry_fav_icon')): ?>
			<link rel="shortcut icon" href="<?php echo get_option('dry_fav_icon'); ?>" /> 
		<?php endif; ?>
        
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <?php
        if (!isset($content_width))
            $content_width = 960;
        ?>
        <table class="doc-loader">
            <tr>
                <td>
                    <img src="<?php echo get_template_directory_uri(); ?>/images/ajax-document-loader.gif" alt="Loading..." />
                </td>
            </tr>
        </table>
        <!--Navigation Menu -->
        <div class="main-menu absolute">
            <nav class="center-relative main-menu-container">
                <aside class="left">
                    <?php wp_nav_menu(array('theme_location'=>'custom_menu', 'menu_id'=>'menu-custom_menu')); ?>
                </aside>
            </nav>
            <nav class="center-relative sticky-menu-container">
                <aside class="menu">
                    <?php wp_nav_menu(array('theme_location'=>'sticky', 'menu_id'=>'menu-sticky')); ?>
                </aside>
                <div class="logo-slogan">
                    <span><?php echo get_option('blogdescriptionlong'); ?></span>
                </div>
                <aside class="contact-buttons">
                    <div class="inner">
                        <ul>
                            <li id="contact-button-fb">
                                <div id="fb-root"></div>
                                <script>(function(d, s, id) {
                                  var js, fjs = d.getElementsByTagName(s)[0];
                                  if (d.getElementById(id)) return;
                                  js = d.createElement(s); js.id = id;
                                  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
                                  fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>
                                <div class="fb-like" data-href="https://www.facebook.com/dejuistekoers" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
                            </li>
                            <li id="contact-button-twitter">
                                <a href="https://twitter.com/MichaelBreedt" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @MichaelBreedt</a>
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                            </li>
                            <li><a href="<?php echo get_bloginfo('url') ; ?>/#contact">contact</a></li>
                        </ul>
                    </div>
                </aside>
            </nav>
        </div>  
        <!-- End of Navigation Menu -->