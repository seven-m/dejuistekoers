<?php
/**
 * The template used for displaying page content - blog
 *
 * @package WordPress
 * @subpackage Kronos-WP-child
 */

//Get custom field values
$show_page_title = get_post_meta($post->ID, "page_title", true);
$page_title = get_post_meta($post->ID, "page_custom_page_title", true) ? get_post_meta($post->ID, "page_custom_page_title", true) : get_the_title($post);
$page_title_description = get_post_meta($post->ID, "page_title_description", true);
$page_title_color = get_post_meta($post->ID, "page_title_color", true);

$slug = $post->ID;
$posts_per_page = 2;

$wp_query->query('post_type=post&paged=' . $paged);
?>

<?php if (have_posts()) : ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php if ($show_page_title != 'no') : ?>
            <header class="entry-header page-title">
                <h2 class="page-title entry-title"><?php echo $page_title; ?></h2>
                <div class="title-description"><?php echo $page_title_description; ?></div>
            </header><!-- End of entry-header -->
        <?php endif; ?>
        <div id="main-blog-holder">
            <div id="blog-items-holder">
                <div class="entry-content">
                    <ul id="news-items">
                        <?php while (have_posts()) : the_post();
                                $content = get_post_meta($post->ID, 'page_excerpt', substrwords(get_the_content(), 220));

                                if (has_post_thumbnail($post->ID)) {
                                    $thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
                                } else {
                                    $thumb_url = array(get_template_directory_uri().'/images/no_photo.jpg');
                                }
                        ?>
                            <li>
                                <div class="inner">
                                    <article class="<?php echo post_class(); ?>">
                                        <header>
                                            <div class="entry-category category block">
                                                <?php
                                                $cat_list = '';
                                                foreach ((get_the_category()) as $category) {
                                                    if ($category->cat_name != 'Uncategorized') {
                                                        $cat_list .= ' ' . $category->cat_name . ',';
                                                    }
                                                }
                                                if ($cat_list != '') {
                                                    $cat_list .=';';
                                                    $cat_list = explode(',;', $cat_list);
                                                    if ($cat_list[0] != '') {
                                                        echo $cat_list[0];
                                                    }
                                                }
                                                ?>
                                            </div>
                                            <div class="entry-title">
                                                <a class="no-background-color" href="<?php echo get_permalink($post->ID); ?>"><?php echo get_the_title(); ?></a>
                                            </div>
                                            <div class="entry-thumbnail" style="background-image:url(<?php echo $thumb_url[0]; ?>)">
                                                <span class="date">
                                                    <?php echo get_the_time('d F Y', $post->ID); ?> 
                                                </span>
                                            </div>
                                        </header>
                                        <div class="blog-front-content">
                                            <?php echo $content; ?>
                                            <a class="read-more-link" href="<?php echo get_permalink($post->ID) ?>">
                                                &nbsp;<?php echo __('Lees meer', 'Kronos-wp-child'); ?>
                                            </a>
                                        </div>
                                    </article>
                                </div>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                    <div id="blog-pagination" class="clear center-text">   
                        <?php posts_nav_link(' ', '<span class="next">Next</span>', '<span class="previous">Previous</span>'); ?>	
                        <div class="clear"></div>
                    </div>
        <?php endif; ?>
                </div>
                
            </div><!-- end #blog-items-holder -->
        </div><!-- end #main-blog-holder -->
    </article>